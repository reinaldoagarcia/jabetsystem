﻿using FunerariaProyecto.Models;
using System.Collections.Generic;

namespace FunerariaProyecto.ViewModels
{
    public class FacturaView
    {
         public float cantidad;
        public float precio;
        public string estado;

        public string Numero { get; set; }
        public decimal Monto {  get { return (decimal)cantidad * (decimal)precio; } }
        public Cliente Cliente { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteSucursal { get; set; }
        public string ClientePlan { get; set; }
        public int ClienteId { get; set; }
        public int ProductoId { get; set; }
        public int FacturaId { get; set; }
        public string Descripcion { get; set; }

        public ProductFactura Products { get; set; }
        public List<ProductFactura> Product { get; set; }

    }
}